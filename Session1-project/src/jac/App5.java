package jac;

class Machine{
	private String name;
	private String manufacturer;
	private int year;
	
	//default constructor
	public Machine() {
//		setName("default Machine");
//		setManufacturer("Apple");
//		setYear(2012);
		
		this("default Machine", "Apple", 2012);
	}
	
	//Overloading the constructor
	public Machine(String name, String manufacturer) {
//		setName(name);
//		setManufacturer(manufacturer);
//		setYear(2012);
		this(name, manufacturer, 2012);
	}
	
	public Machine(String name, String manufacturer, int year) {
		setName(name);
		setManufacturer(manufacturer);
		setYear(year);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "Machine [name=" + name + ", manufacturer=" + manufacturer + ", year=" + year + "]";
	}
	
	
}


public class App5 {
	public static void main(String...strings) {
		Machine machine1 = new Machine();
		System.out.println(machine1);
		
		Machine machine2 = new Machine("copy machine", "Epson");
		System.out.println(machine2);
		
		Machine machine3 = new Machine("Printer", "Fake company", 1998);
		System.out.println(machine3);

	}
}
