package jac;

class Person extends Object{
	String name;
	
	public Person(String name) {
		this.name = name;
	}

	@Override //annotation optional
	public String toString() {
		return "the name of the person is " + name;
	}
	
	//override the method in the parent => object class
	
}

public class App4 {
	public static void main(String[] argssss) {
		//Object Reference Variable
		
		int x = 10; //primitive data type
		System.out.println(x);
		
		//reference variable
		Person p1 = new Person("Reza");
		
		System.out.println(p1);
		
		Person p2= new Person("Betty");
		p2 = p1;
		
		System.out.println(p2);
		
		Person p3 = p1;
	}
}
